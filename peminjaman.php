<?php
include "koneksi.php";
?>
  <!DOCTYPE html>
<html>
<head>
  <title></title>
  <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
  <link href="css/bootstrap.css" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="data_table/assets/css/jquery.dataTables.css">
  <div class="panel panel-default">
</head>
<body>

<nav class="navbar navbar-default" >
 <div class="panel-footer" style="background-color: teal"> <img src="skanic.png" style="width : 3%">   INVENSKANIC</div>
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>

    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
       
       
          </ul>
       
      
      <ul class="nav navbar-nav navbar-right">
        <li><a href="index.php">Dashboard</a></li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Pengguna<span class="caret"></span></a>
          <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
    <li><a href="d_admin.php">Admin</a></li>
    <li><a href="d_operator.php">Operator</a></li>
    <li><a href="d_user.php">User</a></li>
  </ul>
           <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Inventaris<span class="caret"></span></a>
          <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
    <li><a href="d_barang.php">Barang</a></li>
    <li><a href="d_ruang.php">ruang</a></li>
    <li><a href="d_jenis.php">Jenis</a></li>
  </ul>
          <li><a href="peminjaman_b.php">Peminjam</a></li>
          <li><a href="pengembalian.php">Pengembalian</a></li>
          <li><a href="#"><i class="glyphicon glyphicon-print"></i> Laporan</a></li>
          
        </li>
        </li>
       
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>

<div id="page-wrapper">
    <div class="graphs">
        <div class="row">
        <div class="panel panel-default">
        <div class="panel-heading"><b><center>Form Peminjam</center></b></div>
        <div class="panel-body">
        <div class="col-lg-12">
        <?php 
        include"koneksi.php";
          $sql = mysqli_query($koneksi,"SELECT * FROM inventaris WHERE id_inventaris = '$_GET[id_inventaris]'");
          $data = mysqli_fetch_array($sql);
         ?>
            <form action="proses_pinjam.php" method="POST" enctype="multipart/form-data" >
                <div class="form-group">
                    <label>Nama Barang</label>
                    <input type="text" class="form-control" name="barang" value="<?php echo $data['nama'] ?>"></input>
                    <input type="hidden" class="form-control" name="id_inventaris" value="<?php echo $data['id_inventaris'] ?>"></input>
                </div>
                <div class="form-group">
                    <label>Jumlah</label>
                    <input type="number" class="form-control" name="jumlah" max="<?php echo $d['jumlah'];?>" value="1" min="1"></input>
                </div>
              
                <div class="form-group">
                    <label>Peminjam</label>
                      <select name="id_pegawai" class="form-control" required="" />
                        <?php 
                            include"koneksi.php";
                            $sql=mysqli_query($koneksi,"SELECT * FROM pegawai");
                            while($tampil=mysqli_fetch_array($sql)){

                        ?>
                        <option value="<?php echo $tampil['id_pegawai'];?>"><?php echo $tampil['nama_pegawai'];?></option>
                        <?php 
                        } ?>
                        
                    </select>
                </div>
                <div class="form-group">
                        <input type="Submit" name="simpan" class="btn btn-primary" value="simpan" >
                </div>
            </form>
        </div>
        </div>
        </div>
        </div>
        </div>
     <div class="panel panel-default">
        <div class="panel-heading"><b><center>DATA INVENTARIS</center></b></div>
        <div class="panel-body">
        <br>
        <div class="table-responsive">
            <table id="dataTables-example" class="table table-bordered table-hover table-striped">
                <thead>
                    <tr>
                        <td>No</td>
                        <td>Nama Barang</td>
                        <td>Tanggal Pinjam</td>
                        <td>Status</td>
                        <td>Peminjam</td>
                        <td>Aksi</td>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $no=1;
                    $pilih=mysqli_query($koneksi, "SELECT * FROM peminjaman p LEFT JOIN inventaris i ON p.id_inventaris=i.id_inventaris where p.status_peminjaman = 'pinjam' order by p.id_peminjaman asc");
                    while($data=mysqli_fetch_array($pilih)){
                        $id_pegawai = $data['id_pegawai'];
                        $q_data_pegawai = mysqli_query($koneksi, "select * from pegawai where id_pegawai ='$id_pegawai'");
                        $data_pegawai= mysqli_fetch_array($q_data_pegawai);
                    ?>
                    <tr>
                        <td><?=$no++; ?></td>
                        <td><?=$data['nama'];?></td>
                        <td><?=$data['tanggal_pinjam'];?></td>
                        <td><?=$data['status_peminjaman'];?></td>
                        <td><?=$data_pegawai['nama_pegawai'];?></td>
                        <td>
                            <a class="btn btn-success" href="proses_kembali.php?id_peminjaman=<?php echo $data['id_peminjaman'];?>">Kembalikan</a>
                        </td>
                    </tr>
                    <?php } ?>
                </tbody>
            </table>
           
        </div>
    </div>

    </tbody>
  </table>
</div>

</div>
<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript" src="data_table/assets/js/jquery.min.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="data_table/assets/js/jquery.dataTables.min.js"></script>
<script>
  $(document).ready(function(){
    $('#example').DataTable();
  });
</script>
</body>
</html>