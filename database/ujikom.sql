-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Mar 09, 2019 at 05:02 AM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 5.6.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ujikom`
--

-- --------------------------------------------------------

--
-- Table structure for table `detail_pinjam`
--

CREATE TABLE `detail_pinjam` (
  `id_detail_pinjam` int(15) NOT NULL,
  `id_inventaris` int(15) NOT NULL,
  `jumlah` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `inventaris`
--

CREATE TABLE `inventaris` (
  `id_inventaris` int(15) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `kondisi` varchar(500) NOT NULL,
  `keterangan` varchar(500) NOT NULL,
  `jumlah` varchar(50) NOT NULL,
  `id_jenis` int(15) NOT NULL,
  `tanggal_register` datetime NOT NULL,
  `id_ruang` int(15) NOT NULL,
  `kode_inventaris` varchar(15) NOT NULL,
  `sarana` varchar(100) NOT NULL,
  `gambar` varchar(100) NOT NULL,
  `id_petugas` int(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `inventaris`
--

INSERT INTO `inventaris` (`id_inventaris`, `nama`, `kondisi`, `keterangan`, `jumlah`, `id_jenis`, `tanggal_register`, `id_ruang`, `kode_inventaris`, `sarana`, `gambar`, `id_petugas`) VALUES
(10, 'laptop', 'baik', 'tersedia', '100', 4, '2019-02-26 09:11:55', 3, '123', 'RPL', '', 0),
(11, 'obeng', 'kurang aik', 'pelpro', '25', 4, '2019-03-01 13:22:57', 3, '1234', 'RPL', '', 0),
(12, 'tang', 'baik', 'tersedia', '12', 4, '2019-02-26 09:21:46', 3, '12345', 'TKR', '', 0),
(13, 'kacamata', 'baik', 'pel pro', '30', 4, '2019-03-06 07:38:50', 3, '1281', 'TPL', '', 0),
(14, 'wig', 'baik', 'pel pro', '5', 4, '2019-03-08 13:43:26', 3, '5355', 'Animasi', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `jenis`
--

CREATE TABLE `jenis` (
  `id_jenis` int(15) NOT NULL,
  `nama_jenis` varchar(50) NOT NULL,
  `kode_jenis` varchar(15) NOT NULL,
  `keterangan` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jenis`
--

INSERT INTO `jenis` (`id_jenis`, `nama_jenis`, `kode_jenis`, `keterangan`) VALUES
(4, 'elektro', '3', 'pel pro'),
(5, 'kom', '4', 'pelpro');

-- --------------------------------------------------------

--
-- Table structure for table `level`
--

CREATE TABLE `level` (
  `id_level` int(15) NOT NULL,
  `nama_level` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `level`
--

INSERT INTO `level` (`id_level`, `nama_level`) VALUES
(1, 'admin'),
(2, 'petugas');

-- --------------------------------------------------------

--
-- Table structure for table `pegawai`
--

CREATE TABLE `pegawai` (
  `id_pegawai` int(15) NOT NULL,
  `nama_pegawai` varchar(20) NOT NULL,
  `nip` int(15) NOT NULL,
  `alamat` varchar(50) NOT NULL,
  `id_level` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pegawai`
--

INSERT INTO `pegawai` (`id_pegawai`, `nama_pegawai`, `nip`, `alamat`, `id_level`) VALUES
(1, 'annisa', 1234, 'bogor', 3);

-- --------------------------------------------------------

--
-- Table structure for table `peminjaman`
--

CREATE TABLE `peminjaman` (
  `id_peminjaman` int(15) NOT NULL,
  `tanggal_pinjam` datetime NOT NULL,
  `tanggal_kembali` datetime NOT NULL,
  `status_peminjaman` varchar(50) NOT NULL,
  `id_pegawai` int(15) NOT NULL,
  `id_inventaris` int(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `peminjaman`
--

INSERT INTO `peminjaman` (`id_peminjaman`, `tanggal_pinjam`, `tanggal_kembali`, `status_peminjaman`, `id_pegawai`, `id_inventaris`) VALUES
(1, '2019-03-01 00:00:00', '2019-03-01 04:28:05', 'Dikembalikan', 1, 10),
(2, '2019-03-01 00:00:00', '0000-00-00 00:00:00', 'Pinjam', 1, 10),
(3, '2019-03-01 00:00:00', '2019-03-08 05:32:39', 'Dikembalikan', 1, 0),
(4, '2019-03-01 00:00:00', '2019-03-01 04:41:07', 'Dikembalikan', 1, 0),
(5, '2019-03-01 00:00:00', '0000-00-00 00:00:00', 'Pinjam', 1, 10),
(6, '2019-03-01 00:00:00', '0000-00-00 00:00:00', 'Pinjam', 1, 10),
(7, '2019-03-01 00:00:00', '2019-03-04 06:59:36', 'Dikembalikan', 1, 10),
(8, '2019-03-01 00:00:00', '2019-03-01 07:20:37', 'Dikembalikan', 1, 10),
(9, '2019-03-01 00:00:00', '2019-03-01 05:31:30', 'Dikembalikan', 1, 10),
(10, '2019-03-01 00:00:00', '2019-03-01 07:14:52', 'Dikembalikan', 1, 10),
(11, '2019-03-04 00:00:00', '0000-00-00 00:00:00', 'Pinjam', 1, 11),
(12, '2019-03-08 00:00:00', '0000-00-00 00:00:00', 'Pinjam', 1, 12),
(13, '2019-03-08 00:00:00', '0000-00-00 00:00:00', 'Pinjam', 1, 10),
(14, '2019-03-08 00:00:00', '0000-00-00 00:00:00', 'Pinjam', 1, 10);

-- --------------------------------------------------------

--
-- Table structure for table `petugas`
--

CREATE TABLE `petugas` (
  `id_petugas` int(15) NOT NULL,
  `username` varchar(20) NOT NULL,
  `password` varchar(20) NOT NULL,
  `nama_petugas` varchar(20) NOT NULL,
  `id_level` int(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `petugas`
--

INSERT INTO `petugas` (`id_petugas`, `username`, `password`, `nama_petugas`, `id_level`) VALUES
(12345, 'annisa', 'annisa', 'annisa', 12345),
(12355, 'nisa', 'annisa', 'fgf', 1),
(12356, 'unang', 'nisa', 'unang', 2);

-- --------------------------------------------------------

--
-- Table structure for table `ruang`
--

CREATE TABLE `ruang` (
  `id_ruang` int(15) NOT NULL,
  `nama_ruang` varchar(20) NOT NULL,
  `kode_ruang` int(15) NOT NULL,
  `keterangan` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ruang`
--

INSERT INTO `ruang` (`id_ruang`, `nama_ruang`, `kode_ruang`, `keterangan`) VALUES
(3, 'lab1', 1, 'pelpro'),
(4, 'lab2', 2, 'pel pro');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `detail_pinjam`
--
ALTER TABLE `detail_pinjam`
  ADD PRIMARY KEY (`id_detail_pinjam`);

--
-- Indexes for table `inventaris`
--
ALTER TABLE `inventaris`
  ADD PRIMARY KEY (`id_inventaris`);

--
-- Indexes for table `jenis`
--
ALTER TABLE `jenis`
  ADD PRIMARY KEY (`id_jenis`);

--
-- Indexes for table `level`
--
ALTER TABLE `level`
  ADD PRIMARY KEY (`id_level`);

--
-- Indexes for table `pegawai`
--
ALTER TABLE `pegawai`
  ADD PRIMARY KEY (`id_pegawai`);

--
-- Indexes for table `peminjaman`
--
ALTER TABLE `peminjaman`
  ADD PRIMARY KEY (`id_peminjaman`);

--
-- Indexes for table `petugas`
--
ALTER TABLE `petugas`
  ADD PRIMARY KEY (`id_petugas`);

--
-- Indexes for table `ruang`
--
ALTER TABLE `ruang`
  ADD PRIMARY KEY (`id_ruang`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `detail_pinjam`
--
ALTER TABLE `detail_pinjam`
  MODIFY `id_detail_pinjam` int(15) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `inventaris`
--
ALTER TABLE `inventaris`
  MODIFY `id_inventaris` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `jenis`
--
ALTER TABLE `jenis`
  MODIFY `id_jenis` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `level`
--
ALTER TABLE `level`
  MODIFY `id_level` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `pegawai`
--
ALTER TABLE `pegawai`
  MODIFY `id_pegawai` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `peminjaman`
--
ALTER TABLE `peminjaman`
  MODIFY `id_peminjaman` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `petugas`
--
ALTER TABLE `petugas`
  MODIFY `id_petugas` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12357;
--
-- AUTO_INCREMENT for table `ruang`
--
ALTER TABLE `ruang`
  MODIFY `id_ruang` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
