<?php
  session_start();
  if ($_SESSION['level_user']!="Admin") {
    echo "<script>alert('Tidak Login');window.location.href='login/index.php'</script>";
  }
?>
<!DOCTYPE html>
<html>
<head>
	<title>INVENSKANIC</title>
	<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
	<link href="css/bootstrap.css" rel="stylesheet">
	<div class="panel panel-default">
</head>
<body>

<nav class="navbar navbar-default" >

 <div class="panel-footer" style="background-color: teal"> <img src="skanic.png" style="width : 3%"> INVENSKANIC
 <span style="float: right;"><a href="logout.php"><i class="glyphicon glyphicon-log-out"></i></a></span>
 </div>

  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      
      <ul class="nav navbar-nav navbar-right">
        <li><a href="index.php">Dashboard</a></li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Pengguna<span class="caret"></span></a>
          <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
        <li><a href="d_admin.php">Admin</a></li>
        <li><a href="d_operator.php">Operator</a></li>
        <li><a href="d_user.php">User</a></li>
      </ul>
           <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> Inventaris<span class="caret"></span></a>
          <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
    <li><a href="d_barang.php">Barang</a></li>
    <li><a href="d_ruang.php">Ruang</a></li>
    <li><a href="d_jenis.php">Jenis</a></li>
    
  </ul>
          <li><a href="peminjaman_b.php">Peminjam</a></li>
          <li><a href="pengembalian.php">Pengembalian</a></li>
           <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Laporan<span class="caret"></span></a>
          <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
    <li><a href="laporan_barang.php">Data Barang</a></li>
    <li><a href="laporan_peminjaman.php">Data peminjaman</a></li>
    <li><a href="laporan_pengembalian.php">Data Pengembalian</a></li>
          
        </li>
        
        </li>
       
      </ul>
       <li><a href="backup_database.php">BackupDatabase</a></li>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>